FROM python:slim

ADD requirements.txt /code/requirements.txt

WORKDIR /code

RUN pip install -r requirements.txt

ADD src/. /code

RUN mkdir /data && chown www-data /data

USER www-data

CMD python app.py
