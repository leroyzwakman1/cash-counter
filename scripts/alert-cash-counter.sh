#!/bin/bash

csv_file="/var/lib/docker/volumes/cash_counter_data/_data/bedragen.csv"
treshhold=10
source ~/config

# read the csv file into vars
function parse() {

  msg=""
  prio=default
  while IFS="," read -r datum tijd type vijftig twintig tien vijf twee een half vijfde tiende twintigste totaal locatie; do

    echo "$msg ${datum} @ ${tijd} ${totaal} " > /var/tmp/$1

    if [ $twee -lt $treshhold ]; then
      echo "2.00 -> ${twee}" >> /var/tmp/$1
      prio=high
    fi

    if [ $een -lt $treshhold ]; then
      echo "1.00 -> ${een}" >> /var/tmp/$1
      prio=high
    fi

    if [ $half -lt $treshhold ]; then
      echo "0.50 -> ${half}" >> /var/tmp/$1
      prio=high
    fi

    if [ $vijfde -lt $treshhold ]; then
      echo "0.20 -> ${vijfde}" >> /var/tmp/$1
      prio=high
    fi

    if [ $tiende -lt $treshhold ]; then
      echo "0.10 -> ${tiende}" >> /var/tmp/$1
      prio=high
    fi

    if [ $twintigste -lt $treshhold ]; then
      echo "0.05 -> ${twintigste}" >> /var/tmp/$1
      prio=high
    fi

    curl \
      -H "Title: $1" \
      -H "Priority: ${prio}" \
      -d "`cat /var/tmp/$1`" \
      ${url}

    echo $prio


    echo -e ${msg}

  done < <( grep -r $1 ${csv_file} | tail -20 | grep eind | sort -r | head -1 )
}
parse "kantine"
parse "baan"

exit
