build:
	docker build -t cash-counter:latest .

debug:
	docker run --rm  -it -e TZ=Europe/Amsterdam -p 8081:5000 -v `pwd`/src:/code  -v cash_counter_data:/data  cash-counter:latest

run:
	docker run -it -p 8080:5000 -e TZ=Europe/Amsterdam -v cash_counter_data:/data --name cash-counter cash-counter:latest

daemon:
	docker run --restart unless-stopped -e TZ=Europe/Amsterdam -d -p 8080:5000 -v cash_counter_data:/data --name cash-counter cash-counter:latest

clean:
	docker rm -f cash-counter

