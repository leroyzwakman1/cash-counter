# Cash-Counter

My first Flask application in Docker

The idea is to count the nominations, and compare them with the last entry.

### Build instructions
```bash
docker build -t cash-counter:latest .
```

### run instructions
```bash
docker run -it -p 8080:5000 --name cash-counter cash-counter:latest
```

### run instructions (debug)
```bash
docker run --rm  -it -p 8080:5000 -v `pwd`/src:/code --name cash-counter cash-counter:latest
```

#### Screenshot
![screenshot](screenshot.png)


## cronjobs

```cron
0 10  * * 0  /usr/local/bin/alert-cash-counter.sh
0 18  * * 2  /usr/local/bin/alert-cash-counter.sh
0 18  * * 4  /usr/local/bin/alert-cash-counter.sh
```
