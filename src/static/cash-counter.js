$(document).ready(function(){

  // select all text for Tabs/Android  when focussed
  $("input").focusin(function(){
    if ( this.type == "text" ) {
      $(this).select();
      return false;
    }
  });

  // if predefined value
  $("input").click(function(){
      if ( this.value == "leegmaken" ) {
        if (confirm("leegmaken?") == true) {
          resetForm();
        }
        else {
           return false;
        }
      }
      else if (this.value == "opslaan" ) {
        return true;
      }
      else {
        calculatePreDefined(this);
        return false;
      }
  });

  // if user changes text field
  $("input").change(function(){
    if (! isNaN(this.value) ) {
      calculatePreDefined(this);
    }
    else {
      this.value = 0;
    }
      return false;
  });
});

function copy2clipboard(element) {
  // Get the text field
  //copyText.setSelectionRange(0, 99999); // For mobile devices

   // Copy the text inside the text field
  navigator.clipboard.writeText(element);

  alert("Copied the text: " + element);

}

function calculatePreDefined(button) {
  //console.log(button);
  var audio = new Audio('/static/tick.mp3');
  audio.play();
  // text fields to target
  value_field = $('#' + button.name);
  sum_field = $('#' + button.name + '_sum');

  // get current value
  value = parseInt(value_field.val());
  value_button = parseInt(button.value);

  // add the value only if button, else the value is alread set!
  if (button.type != "text") {
    value += value_button;
  }

  // check if not less than zero
  if ( value < 0 ) {
    value = 0
  }

  // store value
  value_field.val(value);
  $.ajax('/ajax?' + button.name + '='  + value);


  // set total in row
  sum = parseFloat(value * button.name.replace('_','.'));
  //console.log(sum);
  sum_field.text(sum.toFixed(2));

  calculateTotals();

}
function calculateSubTotals() {

  $( 'div[name="total"]' ).each(function() {
    id = this.id.replace('_sum','');
    coin = id.replace('_','.');
    value = $('#' + id).attr("value");
    sum = coin * value;
    $(this).text(sum.toFixed(2));
  });
  calculateTotals();
}

function calculateTotals() {

  total = 0;
  // fetch all row totals, and add them up
  $( 'div[name="total"]' ).each(function() {
    value = $( this ).html();
    total = total + parseFloat(value);
    //console.log(total);
  });

  // disable the safe button if total = 0
  if ( total > 0) {
    $('#opslaan').prop('disabled', false);
  }
  else {
    $('#opslaan').prop('disabled', true);
  }

  // show total value
  $('#real_total').text(total.toFixed(2));
  $('#real_total_hidden').val(total.toFixed(2));

}

function resetForm() {

  // fill everything with zero

  $( 'input[class="values"]' ).each(function() {
    $(this).attr("value", 0);
    $(this).trigger("click");
  });

}
