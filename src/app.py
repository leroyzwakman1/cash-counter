from flask import Flask
from flask import render_template
from flask import make_response
from flask import request
from flask import session
from flask import redirect
from flask import url_for
from os.path import exists

import csv
import datetime

CSV_FILE='/data/bedragen.csv'
VERSIE='v1.4'


app = Flask(__name__)


@app.route('/')
def keuze():

    create_csv_file()

    locatie = request.cookies.get('locatie')

    if locatie == None:
        return redirect("/locatie", code=302)

    saved = request.args.get("saved")

    todayStartSet = False
    if date_exists("start", locatie):
        todayStartSet = True

    todayStopSet = False
    if date_exists("eind", locatie):
        todayStopSet = True

    resp = make_response(render_template("index.html",
                           today_start_set=todayStartSet,
                           today_end_set=todayStopSet,
                           versie=VERSIE,
                           saved=saved,
                           location=locatie
                           )
                        )

    resp.set_cookie('locatie', locatie,  max_age=60*60*24*365)

    return resp


@app.route('/locatie')
@app.route('/locatie/<name>')
def locatie():

    if request.args.get('location') == "reset":
        resp = make_response(redirect('/'))
        resp.set_cookie('locatie', '',  0)
        return resp

    if not request.cookies.get("locatie") == None:
        return redirect("/", code=302)

    if request.args.get('location'):
        resp = make_response(redirect('/'))
        resp.set_cookie('locatie', request.args.get('location'),  max_age=60*60*24*365)
        return resp

    resp = make_response(render_template("locatie.html",
                           versie=VERSIE,
                           )
                        )
    return resp

@app.route('/ajax')
def ajax():

    if  len(request.args) > 0:
        name_item = next(iter(request.args))
        name_value = request.args.get(name_item)
        resp = make_response()
        resp.set_cookie(name_item, name_value,  max_age=60*60*24*14)
        return resp

    return ""


@app.route('/startbedrag',methods = ['POST', 'GET'])
def startbedag():

    global versie

    locatie = request.cookies.get('locatie')

    #if date_exists("start", locatie):
    #   todayset=True


    if locatie == None:
        return redirect("/locatie", code=302)

    if request.method == 'POST':
        save_data(request, "start")
        return redirect("/?saved=true", code=302)

    return render_template("bedrag.html",
                           type="start",
                           versie=VERSIE,
                           cookies=request.cookies,
                          )


@app.route('/eindbedrag',methods = ['POST', 'GET'])
def eindbedag():

    locatie = request.cookies.get('locatie')

    if locatie == None:
        return redirect("/locatie", code=302)

    if request.method == 'POST':
        save_data(request, "eind")
        return redirect("/?saved=true", code=302)

    return render_template("bedrag.html",
                           type="eind",
                           versie=VERSIE,
                           cookies=request.cookies,
                          )


@app.route('/overzicht')
@app.route('/overzicht/<date>')
def overzicht(date=None):

    create_csv_file()

    starts = []
    ends = []

    locatie = request.cookies.get('locatie')

    if locatie == None:
        return redirect("/locatie", code=302)

    if not date:
        date = datetime.date.today()
    else:
        try:
            date = datetime.datetime.strptime(date, "%Y-%m-%d").date()
        except ValueError:
            return redirect("/overzicht", code=302)

    if date == datetime.date.today():
        next = "disabled"
    else:
        next = date + datetime.timedelta(days=1)

    previous = date - datetime.timedelta(days=1)
    weekDays = ["ma","di","wo","do","vr","za","zo"]


    pretty_date = weekDays[date.weekday()] + " " +  str(date)


    with open(CSV_FILE, encoding='UTF8') as f:
        csv_reader = csv.reader(f, delimiter=',')
        for row in csv_reader:
            if row[0] == "datum":
                headers = row
            if row[0] == str(date):
                if row[2] == "start":
                    if row[14] == locatie:
                        starts.append(row)
                if row[2] == "eind":
                    if row[14] == locatie:
                        ends.append(row)

    return render_template("overzicht.html",
                           headers=headers,
                           date=date,
                           pretty_date=pretty_date,
                           starts=starts,
                           ends=ends,
                           previous=previous,
                           versie=VERSIE,
                           next=next,
                           location=locatie
                           )

def save_data(request, bedragtype):

    locatie = request.cookies.get('locatie')

    if locatie == None:
        return redirect("/locatie", code=302)

    # check if already present
    with open(CSV_FILE, encoding='UTF8') as f:
        csv_reader = csv.reader(f, delimiter=',')
        for row in csv_reader:
            if row[0] == "datum":
                headers = row
            if row[0] == str(datetime.date.today()) and row[14] == locatie and row[2] == bedragtype:
                ongelijk = False
                if row[3] != request.form.get('50'):
                  ongelijk = True
                if row[4] != request.form.get('20'):
                  ongelijk = True
                if row[5] != request.form.get('10'):
                  ongelijk = True
                if row[6] != request.form.get('5'):
                  ongelijk = True
                if row[7] != request.form.get('2_00'):
                  ongelijk = True
                if row[8] != request.form.get('1_00'):
                  ongelijk = True
                if row[9] != request.form.get('0_50'):
                  ongelijk = True
                if row[10] != request.form.get('0_20'):
                  ongelijk = True
                if row[11] != request.form.get('0_10'):
                  ongelijk = True
                if row[12] != request.form.get('0_05'):
                  ongelijk = True

                if ongelijk == False:
                    print("vandaag al zelfde ingevuld!")
                    return
    print("saving")

    with open(CSV_FILE, 'a', encoding='UTF8') as f:
        writer = csv.writer(f)

        data = [0 for i in range(15)]
        data[0] = datetime.date.today()
        data[1] = datetime.datetime.now().strftime("%H:%M:%S")
        data[2] = bedragtype
        data[3] = request.form.get('50')
        data[4] = request.form.get('20')
        data[5] = request.form.get('10')
        data[6] = request.form.get('5')
        data[7] = request.form.get('2_00')
        data[8] = request.form.get('1_00')
        data[9] = request.form.get('0_50')
        data[10] = request.form.get('0_20')
        data[11] = request.form.get('0_10')
        data[12] = request.form.get('0_05')
        data[13] = request.form.get('totaal')
        data[14] = locatie

        # write the data
        writer.writerow(data)

def date_exists(bedragtype, locatie):

    # walk through the csv file to find the date
    with open(CSV_FILE, 'r', encoding='UTF8') as f:
        csvreader = csv.reader(f)
        rows = []
        for row in csvreader:
            if row[0] == str(datetime.date.today()):
                if row[2] == bedragtype:
                    if row[14] == locatie:
                        return True
            rows.append(row)
    return False

def create_csv_file():

    header = ['datum', 'tijd', 'type', '50', '20', '10', '5',
              '2', '1', '0.50', '0.20', '0.10', '0.05', 'totaal', 'locatie']

    # Create header in csv file if the file does not exist yet
    if not exists(CSV_FILE):
        with open(CSV_FILE, 'w', encoding='UTF8') as f:
            writer = csv.writer(f)
            # write the header
            writer.writerow(header)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=False)

